# Source Control

## Tags

### Release Tags

When tagging releases in a version control system, the tag for a version MUST
be "vX.Y.Z" e.g. "v3.1.0". The trag name will be automatically generated by
the Gradle release plugin.

## Branches

### Feature branches

Use the issue number first, e.g.

    git checkout -b issue-15-oauth-migration


### Support branches

In order to maintain multiple major/minor versions at the same time, use 
`support/<major>.<minor>`
