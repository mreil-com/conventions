# Release

## Gradle Plugin

Release a version manually by running

    ./gradlew release

or

    ./gradlew -xcheck release
